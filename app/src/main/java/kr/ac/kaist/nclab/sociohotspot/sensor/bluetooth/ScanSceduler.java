package kr.ac.kaist.nclab.sociohotspot.sensor.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;

import java.util.Set;

import kr.ac.kaist.nclab.sociohotspot.sensor.movement.MovementDetectRepeater;
import kr.ac.kaist.nclab.sociohotspot.sensor.movement.MovementDetector;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;

/**
 * Created by kimwonjung on 2016. 3. 17..
 */
public class ScanSceduler {
    private static final String TAG = ScanSceduler.class.getSimpleName();
    private static final int LONG_PERIOD = 30;
    private static final int SHORT_PERIOD = 10;
    private static int stationaryScanPeriod = 30;
    private static int movingScanPeriod = 30;
   // private BehaviorSubject<Integer>

    //private Observable<Set<BluetoothDevice>> devices;
    private Observable<BluetoothDiscoveryResult> results;
    public ScanSceduler(Context ctx) {
        /*MovementDetector d = new MovementDetector(ctx);
        d.isMoving()
                .map((m)-> m ? SHORT_PERIOD : LONG_PERIOD);*/
        BehaviorSubject<Integer> s = BehaviorSubject.create();
       /* this.devices = BluetoothScanRepeater
                .getRepeater()
                .flatMap((o) -> new BluetoothScanner(ctx).scanDevices());*/
        this.results = BluetoothScanRepeater
                .getRepeater()
                .flatMap((o) -> new BluetoothScanner(ctx).scanDevices());

    }


    public Observable<BluetoothDiscoveryResult> getResults() {
        return results;
    }
}
