package kr.ac.kaist.nclab.sociohotspot.sensor;

import android.app.AlarmManager;
import android.app.Application;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.PowerManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by kimwonjung on 2016. 3. 16..
 */
@Module
public class SystemServiceModule {
    private final Application application;

    public SystemServiceModule(Application application) {
        this.application = application;
    }

    @Provides
    Context provideContext(){
        return application;
    }


    @Provides
    @Singleton
    PowerManager providePowerManager() {
        return (PowerManager)application.getSystemService(Context.POWER_SERVICE);
    }

    @Provides
    @Singleton
    AlarmManager provideAlarmManager() {
        return (AlarmManager)application.getSystemService(Context.ALARM_SERVICE);
    }

    @Provides
    @Singleton
    LocationManager provideLocationManager() {
        return (LocationManager)application.getSystemService(Context.LOCATION_SERVICE);
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences() {
        return (SharedPreferences)application.getSharedPreferences("config", 0);
    }
}
