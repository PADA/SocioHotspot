package kr.ac.kaist.nclab.sociohotspot.logger;

import android.content.Context;
import android.content.SharedPreferences;

import dagger.Module;
import dagger.Provides;
import kr.ac.kaist.nclab.sociohotspot.Config;
import kr.ac.kaist.nclab.sociohotspot.sensor.bluetooth.BluetoothScanner;

/**
 * Created by kimwonjung on 2016. 3. 21..
 */
@Module
public class LoggerModule {
    private Context context;
    public LoggerModule(Context ctx) {
        this.context = ctx;
    }
    @Provides
    FTPManager provideFTPManager(Config config) {
        return new FTPManager(config);
    }
}
