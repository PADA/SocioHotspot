package kr.ac.kaist.nclab.sociohotspot;

import android.app.Application;
import android.content.Context;
import android.location.Location;
import android.util.Log;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import kr.ac.kaist.nclab.sociohotspot.logger.LoggerModule;
import kr.ac.kaist.nclab.sociohotspot.sensor.SensorModule;
import kr.ac.kaist.nclab.sociohotspot.sensor.SocioThermometer;
import kr.ac.kaist.nclab.sociohotspot.sensor.SystemServiceModule;
import kr.ac.kaist.nclab.sociohotspot.sensor.bluetooth.BluetoothScanRepeater;
import kr.ac.kaist.nclab.sociohotspot.sensor.bluetooth.BluetoothScanner;
import kr.ac.kaist.nclab.sociohotspot.sensor.movement.MovementDetectRepeater;
import kr.ac.kaist.nclab.sociohotspot.sensor.movement.MovementDetector;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by kimwonjung on 2016. 3. 16..
 */
public class App extends Application {
    private static final String TAG = App.class.getSimpleName();
    private AppComponent component;
    private static App app;
    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        buildComponentAndInject();

        MovementDetectRepeater.start(this);


        BluetoothScanRepeater.start(this);

        MovementDetector d = new MovementDetector(this);

        Subscription s =  d.isMoving()
                .subscribe((moving) -> {
                    Log.d(TAG, "isMoving: " + moving);
                   // BluetoothScanRepeater.adjustScanPeriod(moving);
                    BluetoothScanRepeater.setIsMoved(moving);

                });

        SocioThermometer.getMonitor()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((result) -> {

                    if (result != null) {


                        try {
                            component.logger().log(result);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.v(TAG, "timeout after 20 seconds");
                    }
                }, (e) -> {
                    e.printStackTrace();
                });
        //SocioThermometer.startMonitoring();
        //Log.v(TAG, "username: " + Config.username);
        //Log.v(TAG, "ground mode: " + Config.groundTruthMode);
    }



    public void buildComponentAndInject() {
        component = DaggerComponentInitializer.init(this);
    }

    public static AppComponent component(Context context) {
        return ((App) context.getApplicationContext()).component;
    }

    public static AppComponent component() {
        return component(app);
    }
    public static Application application() {
        return app;
    }

    //@NoArgsConstructor(access = AccessLevel.PRIVATE)
    public final static class DaggerComponentInitializer {

        public static AppComponent init(App app) {
            return DaggerAppComponent.builder()
                    .systemServiceModule(new SystemServiceModule(app))
                    .sensorModule(new SensorModule(app))
                    .loggerModule(new LoggerModule(app))
                    .build();
        }

    }
}
