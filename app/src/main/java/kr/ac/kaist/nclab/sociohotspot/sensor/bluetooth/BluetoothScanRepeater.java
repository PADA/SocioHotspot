package kr.ac.kaist.nclab.sociohotspot.sensor.bluetooth;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PowerManager;
import android.os.SystemClock;
import android.util.Log;

import kr.ac.kaist.nclab.sociohotspot.App;
import kr.ac.kaist.nclab.sociohotspot.AppComponent;
import kr.ac.kaist.nclab.sociohotspot.Config;
import kr.ac.kaist.nclab.sociohotspot.sensor.movement.MovementDetector;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;

/**
 * Created by kimwonjung on 2016. 3. 17..
 */
public class BluetoothScanRepeater extends BroadcastReceiver{
    public static final int MOVEMENT_CHECK_DURATION = 5;
    private static final String TAG = BluetoothScanRepeater.class.getSimpleName();
    private static int SENSING_PERIOD = 10;
    private static PublishSubject<Object> schedule = PublishSubject.create();


    private static int nextPeriod = 30;
   // private static int stationaryScanPeriod = 60 * 60 * 2;
    //private static int movingScanPeriod = 60 * 60 * 2;
    public static boolean isMoved = false;

    public static void start(final Context context) {

        //PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        //  PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
        // Log.d("ff", "acquire(): duty cycle: " + DUTY_CYCLE + ", WAKELOCK_TIME: " + WAKELOCK_TIME + ", count left: " + count);
        //    wl.acquire();
        // wl.acquire(WAKELOCK_TIME);
        // SystemClock.sleep(WAKELOCK_TIME);
        // Put here YOUR code.
        //  Toast.makeText(context, "Alarm !!!!!!!!!!", Toast.LENGTH_LONG).show(); // For example
        AppComponent comp = App.component();
        AlarmManager am = comp.alarmManager();

        cancel(context);


        Intent i = new Intent(context, BluetoothScanRepeater.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
        //am.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + DUTY_CYCLE + offset, pi);
        //  am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), SENSING_PERIOD * 1000, pi);
        //am.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), pi);
        //am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), pi);

        if (Build.VERSION.SDK_INT >= 19)
            am.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 10, pi);
        else if (Build.VERSION.SDK_INT >= 15)
            am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), nextPeriod * 1000, pi);


    }

    public static void cancel(Context context) {
        Log.v(TAG, "cancel()");

        AppComponent comp = App.component();
        AlarmManager am = comp.alarmManager();
        Intent i = new Intent(context, BluetoothScanRepeater.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
        am.cancel(pi);
    }

    public static void setNextPeriod(int p) {
        if (p < 20) {
            Log.w(TAG, "Should be longer than 20s");
        }
        nextPeriod = p;
    }

    public static void adjustScanPeriod(boolean moving) {
        Config c = App.component().config();
        setNextPeriod(moving ? c.getMovingPeriod() : c.getStationaryPeriod());
    }

    public static void setIsMoved(boolean m) {
        if (isMoved) {
            return;
        }
        isMoved = m;

    }

   /*public static int getMovingScanPeriod() {
        return ;
    }

    public static int getStationaryScanPeriod() {
        return stationaryScanPeriod;
    }

    public static void setStationaryScanPeriod(int period) {
       stationaryScanPeriod = period;
    }

    public static void setMovingScanPeriod(int period) {
        movingScanPeriod = period;
    }

*/
   public static Observable<Object> getRepeater() {
       return schedule;
   }
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive");
        schedule.onNext(null);
        AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

        Intent i = new Intent(context, this.getClass());
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
        //am.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + SENSING_PERIOD * 1000, pi);
     //   am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + nextPeriod * 1000, pi);
        AppComponent comp = App.component(context);
        Config  config = comp.config();
        nextPeriod = config.getGroundtruthMode() ? 30 : (isMoved ? config.getMovingPeriod() : config.getStationaryPeriod());

        isMoved = false;

        if (Build.VERSION.SDK_INT >= 19)
            am.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + nextPeriod * 1000, pi);

    }

}
