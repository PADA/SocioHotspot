package kr.ac.kaist.nclab.sociohotspot;

import android.app.AlarmManager;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import kr.ac.kaist.nclab.sociohotspot.logger.FTPManager;
import kr.ac.kaist.nclab.sociohotspot.logger.Logger;
import kr.ac.kaist.nclab.sociohotspot.logger.LoggerModule;
import kr.ac.kaist.nclab.sociohotspot.sensor.SocioThermometer;
import kr.ac.kaist.nclab.sociohotspot.sensor.bluetooth.BluetoothScanRepeater;
import kr.ac.kaist.nclab.sociohotspot.sensor.bluetooth.BluetoothScanner;
import kr.ac.kaist.nclab.sociohotspot.sensor.SensorModule;
import kr.ac.kaist.nclab.sociohotspot.sensor.SystemServiceModule;
import kr.ac.kaist.nclab.sociohotspot.sensor.location.LocationObserver;
import kr.ac.kaist.nclab.sociohotspot.sensor.movement.MovementDetector;
import kr.ac.kaist.nclab.sociohotspot.sensor.movement.SensorObserver;

/**
 * Created by kimwonjung on 2016. 3. 16..
 */
@Singleton
@Component(modules={SystemServiceModule.class, SensorModule.class, LoggerModule.class})
public interface AppComponent {
    //SensorComponent newSensorComponent(SensorModule sensorModule);
    Context applicationContext();

    void inject(SensorObserver observer);
    void inject(MovementDetector detector);
    void inject(BluetoothScanner scanner);
    BluetoothScanner bluetoothScanner();
    LocationObserver locationObserver();
    SocioThermometer socioThermometer();
    AlarmManager alarmManager();
    Config config();
    Logger logger();

}
