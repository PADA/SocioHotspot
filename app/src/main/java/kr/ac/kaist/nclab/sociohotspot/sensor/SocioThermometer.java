package kr.ac.kaist.nclab.sociohotspot.sensor;

import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import javax.inject.Inject;

import kr.ac.kaist.nclab.sociohotspot.App;
import kr.ac.kaist.nclab.sociohotspot.AppComponent;
import kr.ac.kaist.nclab.sociohotspot.Config;
import kr.ac.kaist.nclab.sociohotspot.sensor.bluetooth.BluetoothDiscoveryResult;
import kr.ac.kaist.nclab.sociohotspot.sensor.bluetooth.BluetoothScanRepeater;
import kr.ac.kaist.nclab.sociohotspot.sensor.bluetooth.BluetoothScanner;
import kr.ac.kaist.nclab.sociohotspot.sensor.location.LocationObserver;
import rx.Observable;
import rx.subjects.PublishSubject;
import com.google.common.base.Preconditions;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * Created by kimwonjung on 2016. 3. 20..
 */
public class SocioThermometer {
    private static Observable<Result> monitor;

    private Observable<Result> result;

    @Inject
    public SocioThermometer() {
        AppComponent comp = App.component();
        BluetoothScanner scanner = comp.bluetoothScanner();
        LocationObserver locationObserver = comp.locationObserver();

        this.result = Observable.combineLatest(
                scanner.scanDevices(),
                locationObserver.getLocation().timeout(22, TimeUnit.SECONDS, Observable.just(null)), Result::new);

    }

    public Observable<Result> getResult() {
        return result;
    }
    public static class Result {
        BluetoothDiscoveryResult bluetoothDiscoveryResult;
        Location location;
        public Result(BluetoothDiscoveryResult r, @Nullable Location l) {
            this.bluetoothDiscoveryResult =  Preconditions.checkNotNull(r);
            this.location =  l;


        }

        public BluetoothDiscoveryResult getBluetoothDiscoveryResult() {
            return bluetoothDiscoveryResult;
        }

        public Location getLocation() {
            return location;
        }


    }

    public static void startMonitoring() {
        if (monitor != null) {
            return;
        }

        AppComponent comp = App.component();

        Observable<Result> r = BluetoothScanRepeater
                .getRepeater()
                .flatMap((o) -> comp.socioThermometer().getResult());


        monitor = r.share();


    }

    public static Observable<Result> getMonitor(){
        if (monitor == null) {
            startMonitoring();
        }
        return monitor;
    }


}
