package kr.ac.kaist.nclab.sociohotspot;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import dagger.Component;
import kr.ac.kaist.nclab.sociohotspot.logger.Logger;

/**
 * Created by kimwonjung on 2016. 3. 25..
 */
public class ReportActivity extends AppCompatActivity {

    @Bind(R.id.listview_file)
    ListView fileListView;
    ArrayAdapter<File> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        ButterKnife.bind(this);
        AppComponent comp = App.component();
        ;

        File[] files = comp.logger().getLogs();
        adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, files);

        fileListView.setAdapter(adapter);
        fileListView.setOnItemClickListener((AdapterView<?> parent, View view, int pos, long id)-> {
            File f = files[pos];

            try {
                comp.logger()
                        .report(f)
                        .subscribe((o) -> {
                            Snackbar.make(view, "Uploaded successfully", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        }, (e)->{
                            Snackbar.make(view, "failed while uploading: " + e.getLocalizedMessage(), Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        });
            } catch (IOException e) {
                e.printStackTrace();
                Snackbar.make(view, "Failed while opening the log file", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    }


}
