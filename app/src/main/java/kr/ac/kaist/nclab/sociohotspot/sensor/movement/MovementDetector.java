package kr.ac.kaist.nclab.sociohotspot.sensor.movement;

import android.content.Context;
import android.os.PowerManager;
import android.util.Log;

import kr.ac.kaist.nclab.sociohotspot.App;
import kr.ac.kaist.nclab.sociohotspot.AppComponent;
import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by kimwonjung on 2016. 3. 15..
 */
public class MovementDetector {
    private static final int ACTIVE_DURATION = 2;
    private static final int DETECTION_PERIOD = 10;
    Context context;

    private static final String TAG = "MovementDetector";


    private PublishSubject<Object> schedule = PublishSubject.create();
    private Observable<Boolean> isMoving;

    public MovementDetector(final Context ctx) {

        AppComponent comp = App.component(ctx);
        this.context = ctx;
        comp.inject(this);
        /*
        check
                .flatMap(new Func1<PowerManager.WakeLock, Observable<Boolean>>() {
                    @Override
                    public Observable<Boolean> call(final PowerManager.WakeLock wakeLock) {
                        wakeLock.acquire();

                        return SensorObserver.isMoving(ctx, MovementDetectReceiver.MOVEMENT_CHECK_DURATION)
                                .map(new Func1<Boolean, Boolean>() {
                                    @Override
                                    public Boolean call(Boolean isMoving) {
                                        wakeLock.release();

                                        return isMoving;
                                    }
                                });
                    }
                })

                .subscribe((Boolean aBoolean) -> Log.d(TAG, "isMoving: " + aBoolean));*/


        this.isMoving = MovementDetectRepeater
                .getRepeater()
                .flatMap((o) -> new SensorObserver(ctx).isMoving(ACTIVE_DURATION));
                //.subscribe((b)->Log.d(TAG, "isMoving: " + b));


    }

    public Observable<Boolean> isMoving() {
        return isMoving;
    }


}
