package kr.ac.kaist.nclab.sociohotspot.sensor.movement;

/**
 * Created by kimwonjung on 2016. 3. 15..
 */

public class AccelValue {
    public float ax;
    public float ay;
    public float az;

    public AccelValue(float[] values) {
        ax = values[0];
        ay = values[1];
        az = values[2];
    }

    public double size() {
        return Math.sqrt((double) (ax * ax + ay * ay + az * az ));
    }



    public float[] toRaw() {
        return new float[] { ax, ay, az};
    }
}