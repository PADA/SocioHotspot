package kr.ac.kaist.nclab.sociohotspot.sensor.movement;

/**
 * Created by kimwonjung on 2016. 3. 15..
 */

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.PowerManager;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import kr.ac.kaist.nclab.sociohotspot.App;
import kr.ac.kaist.nclab.sociohotspot.AppComponent;
import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by kimwonjung on 2015. 12. 2..
 */
public class SensorObserver implements SensorEventListener {
    private static final String TAG = "SensorObserver";
    private SensorManager manager;
    PublishSubject<AccelValue> values = PublishSubject.create();
    private Context context;
    @Inject PowerManager powerManager;

    public SensorObserver(Context ctx) {
        manager = (SensorManager)ctx.getSystemService(Context.SENSOR_SERVICE);
        Sensor accel = manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        manager.registerListener(this, accel, SensorManager.SENSOR_DELAY_FASTEST);
        this.context = ctx;

        AppComponent comp = App.component(context);
        comp.inject(this);

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        values.onNext(new AccelValue(event.values));
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    private PowerManager.WakeLock getWakeLock() {

        return powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
    }



    public void stop() {
        manager.unregisterListener(this);

    }
    /*
    public static Observable<AccelValue> create(final Context ctx) {
        final PowerManager.WakeLock wakeLock = this.getWakeLock();
        return Observable.using(()-> {
            wakeLock.acquire();
            return new SensorObserver(ctx);
        }, (SensorObserver detector)->detector.values,
            (SensorObserver listener) ->{
                listener.stop();
                wakeLock.release();
            }
        , true);
    }*/

    public Observable<AccelValue> withWakeLock() {
        final PowerManager.WakeLock wakeLock = this.getWakeLock();
        SensorObserver self = this;
        return Observable.using(()-> {
                    wakeLock.acquire();
                    return self;
                }, (SensorObserver detector)->detector.values,
                (SensorObserver o) ->{
                    o.stop();
                    wakeLock.release();
                }
                , true);
    }


    public Observable<Boolean> isMoving(int windowSize) {
        //Log.v(TAG, "isMoving()");
        return this.withWakeLock().take(windowSize, TimeUnit.SECONDS)
                .toList()
                .map(SensorObserver::isMoving);
    }

    private static boolean isMoving(List<AccelValue> accels) {
        double[] accX = new double[accels.size()];
        double[] accY = new double[accels.size()];
        double[] accZ = new double[accels.size()];
        for (int i = 0; i < accels.size(); i++) {
            accX[i] = accels.get(i).ax;
            accY[i] = accels.get(i).ay;
            accZ[i] = accels.get(i).az;
        }

        return isMoving(accX, accY, accZ);
    }

    private static boolean isMoving(double[] accX, double[] accY, double[] accZ) {
        // select minimum-length axis
        int minLength = Math.min(Math.min(accX.length, accY.length), accZ.length);

        for (int i=0; i<minLength; i++) {
            accX[i] /= 9.8;
            accY[i] /= 9.8;
            accZ[i] /= 9.8;
        }

        // get DC = mean(norm_input)
        double totalX = 0.0, totalY = 0.0, totalZ = 0.0;
        for (int i=0; i<minLength; i++) {
            totalX += accX[i];
            totalY += accY[i];
            totalZ += accZ[i];
        }

        double meanX = totalX / minLength;
        double meanY = totalY / minLength;
        double meanZ = totalZ / minLength;

        // get AC = norm_input - DC
        for (int i=0; i<minLength; i++) {
            accX[i] -= meanX;
            accY[i] -= meanY;
            accZ[i] -= meanZ;
        }

        // get ACenergy
        Double[] dataColumns = new Double[minLength];

        for (int i=0; i<minLength; i++) {
            dataColumns[i] = accX[i] * accX[i] + accY[i] * accY[i] + accZ[i] * accZ[i];
        }

        // get mean of ACenergy
        double mean = 0;
        for (double d : dataColumns) {
            mean += d;
        }

        return (mean / minLength) > 0.05;
    }
}