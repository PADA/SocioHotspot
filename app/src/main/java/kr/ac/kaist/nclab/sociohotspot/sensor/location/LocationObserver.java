package kr.ac.kaist.nclab.sociohotspot.sensor.location;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import javax.inject.Inject;

import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;

/**
 * Created by kimwonjung on 2016. 3. 20..
 */
public class LocationObserver implements LocationListener {
    private static final String TAG = LocationObserver.class.getSimpleName();
    private PublishSubject<Location> locationSubject = PublishSubject.create();
    private LocationManager locationManager;

    @Inject
    public LocationObserver(LocationManager locationManager) {
        this.locationManager = locationManager;
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        Log.v(TAG, "requestLocationUpdates()");

    }

    public Observable<Location> getLocation() {
        return locationSubject;
    }

    public void start() {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.v(TAG, "onLocationChanged(): " + location);
        locationManager.removeUpdates(this);
        locationSubject.onNext(location);
        locationSubject.onCompleted();

    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.v(TAG, "onProviderDisabled(): " + provider);
        locationManager.removeUpdates(this);
        locationSubject.onNext(null);
        locationSubject.onCompleted();
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.v(TAG, "onProviderEnabled():  " + provider);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.v(TAG, "onStatusChanged():  " + provider);
    }
}
