package kr.ac.kaist.nclab.sociohotspot.sensor.movement;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PowerManager;
import android.os.SystemClock;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by kimwonjung on 2016. 3. 16..
 */
public class MovementDetectRepeater extends BroadcastReceiver {

    private static final String TAG = MovementDetectRepeater.class.getSimpleName();
    public static final int MOVEMENT_CHECK_DURATION = 5;
    private static int SENSING_PERIOD = 10;
    private static PublishSubject<Void> schedule = PublishSubject.create();


    public MovementDetectRepeater() {

    }

    public static void start(final Context context) {
        AlarmManager am =( AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        //  PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
        // Log.d("ff", "acquire(): duty cycle: " + DUTY_CYCLE + ", WAKELOCK_TIME: " + WAKELOCK_TIME + ", count left: " + count);
        //    wl.acquire();
        // wl.acquire(WAKELOCK_TIME);
        // SystemClock.sleep(WAKELOCK_TIME);
        // Put here YOUR code.
        //  Toast.makeText(context, "Alarm !!!!!!!!!!", Toast.LENGTH_LONG).show(); // For example


        Intent i = new Intent(context, MovementDetectRepeater.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
        //am.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + DUTY_CYCLE + offset, pi);
        //  am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), SENSING_PERIOD * 1000, pi);
        //am.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), pi);
        //am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), pi);
        if (Build.VERSION.SDK_INT >= 19)
            am.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), pi);
        else if (Build.VERSION.SDK_INT >= 15)
            am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), 10 * 1000, pi);
    }

    public static Observable<Void> getRepeater() {
        return schedule;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        //Log.d(TAG, "onReceive");
        schedule.onNext(null);
        AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
       // PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);

        //MovementDetector.check.onNext(wl);

        Intent i = new Intent(context, MovementDetectRepeater.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);

        //am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + SENSING_PERIOD * 1000, pi);


        if (Build.VERSION.SDK_INT >= 19)
            am.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime()  + SENSING_PERIOD * 1000, pi);

    }





}
