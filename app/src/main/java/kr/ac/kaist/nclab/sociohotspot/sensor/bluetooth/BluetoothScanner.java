package kr.ac.kaist.nclab.sociohotspot.sensor.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import kr.ac.kaist.nclab.sociohotspot.App;
import kr.ac.kaist.nclab.sociohotspot.AppComponent;
import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by kimwonjung on 2016. 3. 15..
 */
public class BluetoothScanner {

    private static final String TAG = BluetoothScanner.class.getSimpleName();
    private final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
    private Context context;
    private PublishSubject<BluetoothDiscoveryResult> result = PublishSubject.create();

    private Set<ScanRecord> foundDevices = new HashSet<>();
    private boolean fired = false;
    private final PowerManager.WakeLock wakeLock;
    private long startTimestamp;

    @Inject
    PowerManager powerManager;

    public BluetoothScanner(@NonNull Context ctx) {

        this.context = ctx;
        AppComponent comp = App.component(context);
        comp.inject(this);
        wakeLock = this.getWakeLock();

    }


    /*
    *     public Observable<AccelValue> withWakeLock() {
        final PowerManager.WakeLock wakeLock = this.getWakeLock();
        SensorObserver self = this;
        return Observable.using(()-> {
                    wakeLock.acquire();
                    return self;
                }, (SensorObserver detector)->detector.values,
                (SensorObserver o) ->{
                    o.stop();
                    wakeLock.release();
                }
                , true);
    }


    public Observable<Boolean> isMoving(int windowSize) {
        //Log.v(TAG, "isMoving()");
        return this.withWakeLock().take(windowSize, TimeUnit.SECONDS)
                .toList()
                .map(SensorObserver::isMoving);
    }


    * */

    private PowerManager.WakeLock getWakeLock() {

        return powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
    }


    void start() {
        Log.v(TAG, "start()");
        if (fired) {
            Log.w(TAG, "start(): already fired");
        }
        wakeLock.acquire();
        fired = true;

        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        context.registerReceiver(btReceiver, filter);

        // Register for broadcasts when discovery has finished
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        context.registerReceiver(btReceiver, filter);

        boolean success = btAdapter.startDiscovery();
        startTimestamp = System.currentTimeMillis();
        if (!success) {
            Log.w(TAG, "Bluetooth startDiscovery() failed. Check BT");
        }


    }
    void stop() {
        Log.v(TAG, "end()");
        btAdapter.cancelDiscovery();
        context.unregisterReceiver(btReceiver);
        wakeLock.release();
    }


    public Observable<BluetoothDiscoveryResult> scanDevices() {
      //  BluetoothScanner scanner = new BluetoothScanner();


        return Observable.using(()-> {


                    this.start();

                //    wakeLock.acquire();
                    return this;
                }, (s)->s.result.timeout(20, TimeUnit.SECONDS, Observable.just(getCurrentResult())),
                (s)-> s.stop(), true);
    }


    private BluetoothDiscoveryResult getCurrentResult() {
        List<ScanRecord> list = new ArrayList<>();

        list.addAll(foundDevices);
        BluetoothDiscoveryResult r = new BluetoothDiscoveryResult(startTimestamp, System.currentTimeMillis(),list);
        return r;
    }

    private void onDicoveryFinished() {

    }

    private final BroadcastReceiver btReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
                // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                // If it's already paired, skip it, because it's been listed already

                int rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, Short.MIN_VALUE);
                foundDevices.add(new ScanRecord(rssi, device));
                //Log.v(TAG, "device: " + device);
                //Log.v(TAG, "found: " + device.getName() + " addr: " + device.getAddress());
                if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                   // mNewDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
                }
                // When discovery is finished, change the Activity title
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                    List<ScanRecord> list = new ArrayList<>();

                    list.addAll(foundDevices);
                    BluetoothDiscoveryResult r = new BluetoothDiscoveryResult(startTimestamp, System.currentTimeMillis(),list);

                /*try {
                    Thread.sleep(15000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }*/
                    //devices.onNext(foundDevices);
                    //devices.onCompleted();
                    result.onNext(r);
                    result.onCompleted();
                Log.v(TAG, String.format("discovery finished: %d ms elapsed", System.currentTimeMillis() - startTimestamp));
              /*  setProgressBarIndeterminateVisibility(false);
                setTitle(R.string.select_device);
                if (mNewDevicesArrayAdapter.getCount() == 0) {
                    String noDevices = getResources().getText(R.string.none_found).toString();
                   // mNewDevicesArrayAdapter.add(noDevices);
                }*/
            } else if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                Log.v(TAG, "ACTOION_STATE_CHANGED: ");
            }
        }
    };

    public static class ScanRecord {
        public int rssi;
        public BluetoothDevice device;
        public ScanRecord(int rssi, BluetoothDevice d) {
            this.rssi = rssi;
            this.device = d;
        }

    }
}
