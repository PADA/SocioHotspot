package kr.ac.kaist.nclab.sociohotspot;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import javax.inject.Inject;

/**
 * Created by kimwonjung on 2016. 3. 22..
 */
public class Config {

    //public static boolean groundTruthMode = true;
    //public static final String username = "soowon_friend";
    SharedPreferences prefs;
    private static final String GROUNTTRUTH_KEY = "groundtruth";
    private static final String USERNAME_KEY = "name";
    private static final String MOVING_KEY = "moving_period";
    private static final String STATIONARY_KEY = "stationary_period";

    @Inject
    public Config(SharedPreferences prefs) {
        this.prefs = prefs;
    }


    public boolean setGroundTruthMode(boolean groundTruthMode) {
        //Config.groundTruthMode = groundTruthMode;
        return prefs.edit().putBoolean(GROUNTTRUTH_KEY, groundTruthMode).commit();
    }

    public boolean getGroundtruthMode() {
        return prefs.getBoolean(GROUNTTRUTH_KEY, false);
    }

    public boolean setUsername(String name) {
        return prefs.edit().putString(USERNAME_KEY, name).commit();
    }

    public String getUsername() {
        return prefs.getString(USERNAME_KEY, null);
    }


    public int getMovingPeriod() {
        return prefs.getInt(MOVING_KEY, 7200);
    }

    public boolean setMovingPeriod(int p) {
        return prefs.edit().putInt(MOVING_KEY, p).commit();
    }

    public int getStationaryPeriod() {
        return prefs.getInt(STATIONARY_KEY, 7200);
    }

    public boolean setStationaryPeriod(int p) {
        return prefs.edit().putInt(STATIONARY_KEY, p).commit();
    }


}
