package kr.ac.kaist.nclab.sociohotspot.ui;

import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.support.annotation.IdRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.RadioGroup;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import kr.ac.kaist.nclab.sociohotspot.App;
import kr.ac.kaist.nclab.sociohotspot.AppComponent;
import kr.ac.kaist.nclab.sociohotspot.Config;
import kr.ac.kaist.nclab.sociohotspot.R;

/**
 * Created by kimwonjung on 2016. 3. 26..
 */
public class ConfigActivity extends AppCompatActivity {

    private static final String TAG = ConfigActivity.class.getSimpleName();
    @Bind(R.id.checkbox_groundtruth)
    CheckBox groundtruthCheckbox;

    @Bind(R.id.radio_username)
    RadioGroup usernameRadioGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
        ButterKnife.bind(this);
        AppComponent comp = App.component();
        Config config = comp.config();

        groundtruthCheckbox.setOnClickListener((v) -> {
            boolean checked = groundtruthCheckbox.isChecked();
            boolean success = config.setGroundTruthMode(checked);
            if (success) {
                Snackbar.make(findViewById(android.R.id.content), String.format("SUCCESS: grounttruth mode: %s", config.getGroundtruthMode()), Snackbar.LENGTH_LONG).show();
            } else {
                Snackbar.make(findViewById(android.R.id.content), String.format("FAIL: grounttruth mode: %s", config.getGroundtruthMode()), Snackbar.LENGTH_LONG).show();
            }
        });

        groundtruthCheckbox.setChecked(config.getGroundtruthMode());

        HashMap<String, Integer> map = new HashMap<>();
        map.put("changyoung", R.id.radiobtn_changyoung);
        map.put("heesuk", R.id.radiobtn_heesuk);
        map.put("soowon", R.id.radiobtn_soowon);
        map.put("sunghoon", R.id.radiobtn_sunghoon);
        map.put("soowon_friend", R.id.radiobtn_ssw);

        Integer id = map.get(config.getUsername());

        if (id != null) {
            usernameRadioGroup.check(id);
        }

        usernameRadioGroup.setOnCheckedChangeListener((RadioGroup group, @IdRes int checkedId) -> {
            Log.v(TAG, "checkedId: " + checkedId);
            String name = null;
            switch (checkedId) {
                case R.id.radiobtn_changyoung:
                    name = "changyoung";
                    break;
                case R.id.radiobtn_heesuk:
                    name = "heesuk";
                    break;
                case R.id.radiobtn_soowon:
                    name = "soowon";
                    break;
                case R.id.radiobtn_sunghoon:
                    name = "sunghoon";
                    break;
                case R.id.radiobtn_ssw:
                    name = "soowon_friend";
                    break;
                default:
            }
            boolean success = config.setUsername(name);
            if (success) {
                Snackbar.make(findViewById(android.R.id.content), String.format("SUCCESS: username: %s", config.getUsername()), Snackbar.LENGTH_LONG).show();
            } else {
                Snackbar.make(findViewById(android.R.id.content), String.format("FAIL: username: %s", config.getUsername()), Snackbar.LENGTH_LONG).show();
            }
        });

    }
}
