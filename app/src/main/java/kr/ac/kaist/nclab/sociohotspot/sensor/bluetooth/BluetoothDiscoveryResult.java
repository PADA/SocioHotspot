package kr.ac.kaist.nclab.sociohotspot.sensor.bluetooth;

import java.util.List;

/**
 * Created by kimwonjung on 2016. 3. 20..
 */
public class BluetoothDiscoveryResult {

    private long startTime;
    private long endTime;
    private List<BluetoothScanner.ScanRecord> records;
    public BluetoothDiscoveryResult(long startTime, long endTime, List<BluetoothScanner.ScanRecord> records) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.records = records;
    }


    public List<BluetoothScanner.ScanRecord> getRecords() {
        return records;
    }

    public long getEndTime() {
        return endTime;
    }

    public long getStartTime() {
        return startTime;
    }
    public long getElapsedTime() {
        return endTime - startTime;
    }
}
