package kr.ac.kaist.nclab.sociohotspot;

import android.Manifest;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.ac.kaist.nclab.sociohotspot.sensor.SocioThermometer;
import kr.ac.kaist.nclab.sociohotspot.sensor.bluetooth.BluetoothDiscoveryResult;
import kr.ac.kaist.nclab.sociohotspot.sensor.bluetooth.BluetoothScanRepeater;
import kr.ac.kaist.nclab.sociohotspot.sensor.bluetooth.BluetoothScanner;
import kr.ac.kaist.nclab.sociohotspot.ui.ConfigActivity;
import rx.android.schedulers.AndroidSchedulers;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Bind(R.id.list_device)
    ListView deviceListView;

    @Bind(R.id.edittext_scan_period_stationary)
    EditText stationaryScanPeriodInput;

    @Bind(R.id.edittext_scan_period_moving)
    EditText movingScanPeriodInput;
    private static final int MIN_SCAN_PERIOD = 30;
    ArrayAdapter<String> devicesAdapter;

    @Bind(R.id.textview_lastscan_time)
    TextView lastScanTimeView;

    @Bind(R.id.textview_location)
    TextView locationView;

    @Bind(R.id.textview_current_parameter)
    TextView currentParamterview;

    @Bind(R.id.view_parameter)
    View parameterView;

    ArrayAdapter<String> adapter;
    Config config;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        AppComponent comp = App.component();
        this.config = comp.config();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Snackbar.make(this.findViewById(android.R.id.content), "Location permission required (ACCESS_FINE_LOCATION)", Snackbar.LENGTH_LONG).show();
        }
        // Assign adapter to ListView


        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, new ArrayList<>());
        deviceListView.setAdapter(adapter);

       FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener((view)-> {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
                ;
            try {
                comp.logger()
                        .report()
                        .subscribe((o) -> {
                            Snackbar.make(view, "Uploaded successfully", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        }, (e)->{
                            Snackbar.make(view, "failed while uploading: " + e.getLocalizedMessage(), Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        });
            } catch (IOException e) {
                e.printStackTrace();
                Snackbar.make(view, "Failed while opening the log file", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        /*
        BluetoothScanner scanner = App.component(this)
        .bluetoothScanner();
        Log.d(TAG, "scanner: " + scanner);*/

        /*
        scanner
        .scanDevices()
        .subscribe((devices) -> {
            Log.d(TAG, "devices: " + devices);
            for (BluetoothDevice d : devices) {
                Log.d(TAG, "device: " + d.getName() + " address: " + d.getAddress());
            }
        });*/


        SocioThermometer.getMonitor()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((result) -> {
                    if (result != null) {
                        Log.v(TAG, "result: " + result.getBluetoothDiscoveryResult().getRecords());
                        onBluetoothDiscoveryResult(result.getBluetoothDiscoveryResult());
                        onLocation(result.getLocation());

                    } else {
                        Log.v(TAG, "timeout after 20 seconds");
                    }
                }, (e) -> {
                    e.printStackTrace();
                });

        onParamterChanged(config.getMovingPeriod(), config.getStationaryPeriod());



    }

    @Override
    protected void onResume() {
        super.onResume();
        if (config.getGroundtruthMode()) {
            parameterView.setVisibility(View.GONE);
        }

        if (config.getUsername() == null) {
            Snackbar.make(this.findViewById(android.R.id.content), "Choose your name in Setting menu", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
    }

    private void onParamterChanged(int moving, int stationary) {
        currentParamterview.setText(String.format("moving: %d(s), stationary: %d(s)", moving, stationary));
        movingScanPeriodInput.setText(String.format("%d", moving));
        stationaryScanPeriodInput.setText(String.format("%d", stationary));
    }

    private void onBluetoothDiscoveryResult(BluetoothDiscoveryResult result) {
        SimpleDateFormat sdf = new SimpleDateFormat("h:mm:ss", Locale.KOREA);
        String date = sdf.format(result.getEndTime());
        lastScanTimeView.setText(date);
        //   Log.d(TAG, "devices: " + devices);
        List<String> items = new ArrayList<>();
        for (BluetoothScanner.ScanRecord r : result.getRecords()) {
            BluetoothDevice device = r.device;
            items.add(String.format("%s - %s", device.getName(), device.getAddress()));
        }
        Log.v(TAG, "devices: " + items);
        adapter.clear();
        adapter.addAll(items);
    }

    private void onLocation(Location location) {
        Log.v(TAG, "location: " + location);
        if (location != null) {
            String latlng = String.format("lat: %s, lng: %s", location.getLatitude(), location.getAltitude());
            locationView.setText(latlng);
        } else {
            locationView.setText("GPS timeout after 21s");
        }



    }

    @OnClick(R.id.button_apply)
    public void onApplyButtonClicked() {
        int stationaryPeriod;
        int movingPeriod;
        hideSoftKeyboard();
        try {
            stationaryPeriod = Integer.parseInt(stationaryScanPeriodInput.getText().toString());
            movingPeriod = Integer.parseInt(movingScanPeriodInput.getText().toString());
        } catch (NumberFormatException e) {
            Snackbar.make(this.findViewById(android.R.id.content), "Put numbers in the fields", Snackbar.LENGTH_LONG).show();

            return;
        }

        if (stationaryPeriod < MIN_SCAN_PERIOD || movingPeriod < MIN_SCAN_PERIOD) {
            Snackbar.make(this.findViewById(android.R.id.content), "minimum scan period is 30s", Snackbar.LENGTH_LONG).show();
            return;
        }
        config.setMovingPeriod(movingPeriod);
        config.setStationaryPeriod(stationaryPeriod);
    //    BluetoothScanRepeater.setMovingScanPeriod(movingPeriod);
     //   BluetoothScanRepeater.setStationaryScanPeriod(stationaryPeriod);
        Snackbar.make(this.findViewById(android.R.id.content), String.format("Scan period adjusted: moving: %d(s) stationary: %d(s)", movingPeriod, stationaryPeriod), Snackbar.LENGTH_LONG).show();
        onParamterChanged(movingPeriod, stationaryPeriod);
        BluetoothScanRepeater.start(this);
    }

    private void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, ConfigActivity.class);
            startActivity(intent);
            return true;
        } else if ( id ==R.id.action_report) {
            Intent intent = new Intent(this, ReportActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
