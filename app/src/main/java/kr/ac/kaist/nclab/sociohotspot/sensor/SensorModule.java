package kr.ac.kaist.nclab.sociohotspot.sensor;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import kr.ac.kaist.nclab.sociohotspot.sensor.bluetooth.BluetoothScanner;

/**
 * Created by kimwonjung on 2016. 3. 16..
 */
@Module
public class SensorModule {
    private Context context;
    public SensorModule(Context ctx) {
        this.context = ctx;
    }

    @Provides
    BluetoothScanner provideBluetoothScanner() {
        return new BluetoothScanner(context);
    }


}
