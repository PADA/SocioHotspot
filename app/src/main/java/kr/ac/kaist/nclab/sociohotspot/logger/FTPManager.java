package kr.ac.kaist.nclab.sociohotspot.logger;

import android.util.Log;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import javax.inject.Inject;

import kr.ac.kaist.nclab.sociohotspot.Config;
import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.schedulers.Schedulers;

/**
 * Created by kimwonjung on 2016. 3. 20..
 */
public class FTPManager {

    private static final String TAG = "FTPManager";


    private Config config;
    public void transfer(InputStream stream) throws IOException {
        transfer(stream, generateFilename());
    }

    @Inject
    public FTPManager(Config config) {
        this.config = config;
    }

    private String getRemotePath() {
        return String.format("/%s", config.getUsername());
    }

    private String getFilePrefix() {
        return config.getUsername() + (config.getGroundtruthMode() ? "-ground-" : "");
    }

    public void transfer(InputStream stream, String filename) throws IOException {
        FTPClient ftpClient = new FTPClient();


        ftpClient.connect("143.248.137.225", 11112);
        ftpClient.login("pada_tuning", "01099641189");
        ftpClient.changeWorkingDirectory(getRemotePath());
        ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
        BufferedInputStream buffIn = null;
        buffIn = new BufferedInputStream(stream);
        ftpClient.enterLocalPassiveMode();
        ftpClient.storeFile(filename, buffIn);

        buffIn.close();
        ftpClient.logout();
        ftpClient.disconnect();
        Log.v(TAG, "transfer(): success");
    }



    public Observable<Void> send(String filename) {
        return open(filename).flatMap(this::send);
    }

    public Observable<Void> send(InputStream file) {
        return send(file, generateFilename());
    }

    public Observable<Void> send(InputStream file, String filename) {
        Observable<Void> o = Observable.create((subscriber) -> {
            if (subscriber.isUnsubscribed()) {
                return;
            }

            try {
                transfer(file, filename);
                subscriber.onNext(null);
                subscriber.onCompleted();
            } catch (IOException e) {
                e.printStackTrace();
                subscriber.onError(e);
            }
        });
        return o.subscribeOn(Schedulers.io());
    }

    private Observable<FileInputStream> open(String filename) {
        return Observable.create((subscriber) -> {
            if (subscriber.isUnsubscribed()) {
                return;
            }

            try {
                FileInputStream s = new FileInputStream(filename);
                subscriber.onNext(s);
                subscriber.onCompleted();
            } catch (IOException e) {
                e.printStackTrace();
                subscriber.onError(e);
            }
        });
    }

    private String generateFilename() {
        String uuid = UUID.randomUUID().toString();
        String now = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss", Locale.KOREA).format(new Date());
        return String.format("%s-%s.txt", getFilePrefix(), now);
    }
}
