package kr.ac.kaist.nclab.sociohotspot.util;

import java.util.List;

/**
 * Created by kimwonjung on 2016. 3. 16..
 */
public class MathUtils {

    public static double mean(List<Double> m) {
        double sum = 0;
        for (double c: m) {
            sum += c;
        }
        return sum / m.size();
    }

    public static double variance(List<Double> m) {
        double mean = mean(m);
        double temp = 0;
        for(double a : m)
            temp += (mean-a)*(mean-a);
        return temp/m.size();
    }

    public static double stdev(List<Double> m) {
        return Math.sqrt(variance(m));
    }
}
