package kr.ac.kaist.nclab.sociohotspot.logger;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;

import com.google.common.collect.Lists;

import static com.google.common.base.Preconditions.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

import javax.inject.Inject;

import kr.ac.kaist.nclab.sociohotspot.App;
import kr.ac.kaist.nclab.sociohotspot.sensor.SocioThermometer;
import kr.ac.kaist.nclab.sociohotspot.sensor.bluetooth.BluetoothDiscoveryResult;
import kr.ac.kaist.nclab.sociohotspot.sensor.bluetooth.BluetoothScanner;
import rx.Observable;

import static android.os.BatteryManager.*;


/**
 * Created by kimwonjung on 2016. 3. 18..
 */
public class Logger {

    private final static String filename = "log.csv";
    private Context context;
    private FTPManager ftpManager;

    @Inject
    public Logger(FTPManager ftpManager, Context ctx) {
        this.ftpManager = ftpManager;
        this.context = checkNotNull(ctx);

    }

    private void append(String log) throws IOException {
        try {
            FileOutputStream fos = context.openFileOutput(getFilename(), Context.MODE_APPEND);
            String in = log + "\n\r";
            fos.write(in.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void log(SocioThermometer.Result r) throws IOException {

        try {
            String s = serialize(r).toString();
            append(s);
        } catch (JSONException e) {
            e.printStackTrace();
        }



    }

    public File[] getLogs() {
        return context.getFilesDir().listFiles();
    }

    public Observable<Void> report() throws IOException {

        FileInputStream fis = checkNotNull(context.openFileInput(getFilename()));
        return ftpManager.send(fis);
    }

    public Observable<Void> report(File f) throws IOException {

       // FileInputStream fis = checkNotNull();
        FileInputStream fis = new FileInputStream(f);

        return ftpManager.send(fis, String.format("%s-%s.txt", UUID.randomUUID().toString(), f.getName()));
    }

    private JSONObject serialize(long timestamp, Set<BluetoothScanner.ScanRecord> devices) throws JSONException {

        JSONObject json = new JSONObject();
        json.put("timestamp", timestamp);
        JSONArray ds = new JSONArray();
        for (BluetoothScanner.ScanRecord r: devices) {
            ds.put(serialize(r));
        }
        json.put("devices", ds);
        return json;
    }

    private JSONObject serialize(long timestamp, float batteryLevel, SocioThermometer.Result result) throws JSONException {
        BluetoothDiscoveryResult bt = result.getBluetoothDiscoveryResult();
        JSONObject json = new JSONObject();
        Location loc = result.getLocation();

        if (loc != null) {
            JSONObject geo = new JSONObject();
            double lat = loc.getLatitude();
            double lng = loc.getLongitude();
            geo.put("latitude", lat);
            geo.put("longitude", lng);
            json.put("location", geo);
        } else {
            json.put("location", null);
        }

        json.put("timestamp", timestamp);
        json.put("battery_level", batteryLevel);
        json.put("devices", serialize(bt.getRecords()));
        return json;
    }


    private JSONArray serialize(List<BluetoothScanner.ScanRecord> records) throws JSONException {
        JSONArray ds = new JSONArray();
        for (BluetoothScanner.ScanRecord r: records) {
            ds.put(serialize(r));
        }
        return ds;
    }

    private JSONObject serialize(BluetoothScanner.ScanRecord r) throws JSONException {
        JSONObject json = new JSONObject();
        json.put("rssi", r.rssi);
        BluetoothDevice device = r.device;
        json.put("name", device.getName());
        json.put("address", r.device.getAddress());
        return json;
    }

    private JSONObject serialize(SocioThermometer.Result result) throws JSONException {
        return serialize(currentTimestamp(), getCurrentBatteryLevel(), result);
    }



    private String deviceInfo(BluetoothDevice device) {
        return device.getAddress();
    }

    private static List<String> deivceAddresses( Set<BluetoothDevice> devices) {
        ArrayList<String> addresses = new ArrayList<>();
        for (BluetoothDevice d: devices) {
            addresses.add(d.getAddress());
        }
        return addresses;
    }

    private static long currentTimestamp() {
        return System.currentTimeMillis()/1000;
    }

    private float getCurrentBatteryLevel() {

        Intent batteryIntent = App.application().registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int level = batteryIntent.getIntExtra(EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(EXTRA_SCALE, -1);

        // Error checking that probably isn't needed but I added just in case.
        if(level == -1 || scale == -1) {
            return 50.0f;
        }

        return ((float)level / (float)scale) * 100.0f;
    }

    private String getFilename() {
        String today = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA).format(new Date());
        return String.format("log-%s", today);
    }
}
